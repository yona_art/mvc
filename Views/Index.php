<!DOCTYPE HTML>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="">
    <link rel="icon" href="/MVC/Views/images/favicon.ico" type="image/x-icon">
    <title>Colmar Hotel</title>
    <link rel="stylesheet" type="text/css" href="/MVC/Views/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/MVC/Views/css/style.css">
        <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  </head>
  <body>

    <header class="regular color-2">
      <div class="header-left">
        <img src="/MVC/Views/images/ic-logo.svg" alt="Colmar Symbol">
        <h1><span class="header-span-left">Colmar</span><span class="header-span-right">Hotels</span></h1>
      </div>
      <div class="header-right">
        <nav> 
          <ul>
            <a href="#" id=""><li class="button">Acerca de Nosotros</li></a>
            <a href="/MVC/Views/Signup" id=""><li class="button">Registrate</li></a>
            <a href="/MVC/Views/Login" id=""><li class="button">Ingresa</li></a>
            <a href="/MVC/Views/Room" id=""><li class="button">Agregar Habitacion</li></a>
            <a href="/MVC/Views/Type" id=""><li class="button">Agregar tipo</li></a>
            <a href="/MVC/Views/Reservations" id=""><li class="button">Crear Reservacion</li></a>
            <a href="/MVC/Views/UserReservations" id=""><li class="button">Ver Reservaciones</li></a>
          </ul>
        </nav>
      </div>
    </header>

    <header class="mobile color-2">
      <a href="#"><img src="/MVC/Views/images/ic-logo.svg"></a>
      <a href="#"><img src="/MVC/Views/images/ic-on-campus.svg"></a>
      <a href="#"><img src="/MVC/Views/images/ic-online.svg"></a>
      <a href="#"><img src="/MVC/Views/images/ic-login.svg"></a>
    </header>

<div class="content">

    <div class="main-content">
      <div class="first-section color-1">
        <div class="left">
          <div class="image-container">
            <img src="/MVC/Views/images/resort.jpg" alt="">
          </div>
        </div>
        <div class="right">
          <div class="text-container">
            <h2>Bienvenido a tu casa lejos de casa</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>
            <a href="">
              <div class="start-button button">
                <span>Comenzar Aqui</span>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="second-section color-2">
        <div class="left">
          <div class="image-container">
            <img src="/MVC/Views/images/information-main.jpg" alt="">
          </div>
          <div class="text-container">
            <h2>Descubre algo nuevo el dia de hoy</h2>
            <p>Aqui va mucho texto para motivar y seguir indicando profesionalismo.</p>
          </div>
        </div>
        <div class="right">
          <div class="newsfeed-block">
            <div class="image-container">
              <img src="/MVC/Views/images/information-orientation.jpg" alt="">
            </div>
            <div class="text-container">
              <h3>Fecha Importante</h3>
              <p>Martes 10/11 & Miercoles 10/12: 8am-3pm</p>
              <a href="">Leer Mas</a>
            </div>
          </div>
          <div class="newsfeed-block">
            <div class="image-container">
              <img src="/MVC/Views/images/information-campus.jpg" alt="">
            </div>
            <div class="text-container">
              <h3>Nuestra Ubicacion</h3>
              <p>Descubre cual es tu sucursal mas cercana.</p>
              <a href="">Leer Mas</a>
            </div>
          </div>
          <div class="newsfeed-block color-1">
            <div class="image-container">
              <img src="/MVC/Views/images/information-guest-lecture.jpg" alt="">
            </div>
            <div class="text-container">
              <h3>Evento especial de Lectura</h3>
              <p>Ven y acompañanos a la presentacion del famoso autor y su libro.</p>
              <a href="">Leer Mas</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="courses color-1">
      <div class="text-container">
        <h3>Galeria de Eventos</h3>
      </div>
      <div class="gallery">
        <div class="course-section color-2">
          <img src="/MVC/Views/images/course-software.jpg" alt="Laptop and books">
          <h4>Software engineering</h4>
          <h5>Cursos<h5>
          <h6>Web Development, Mobile Development, iOT, APis</h6>
        </div>
        <div class="course-section color-2">
          <img src="/MVC/Views/images/course-computer-art.jpg" alt="Mobile phone">
          <h4>Arte Computacional</h4>
          <h5>Cursos<h5>
          <h6>Imaging & Design, Web Design, Motion Graphics & Visual Effects,
          Computer Animation</h6>
        </div>
        <div class="course-section color-2">
          <img src="/MVC/Views/images/course-design.jpg" alt="Man holding a camera">
            <h4>Diseño</h4>
          <h5>Cursos<h5>
          <h6>User Experience Design, User Research, Visual Design</h6>
        </div>
        <div class="course-section color-2">
          <img src="/MVC/Views/images/course-data.jpg" alt="Laptop on countertop">
          <h4>Data</h4>
          <h5>Cursos<h5>
          <h6>Data Science, Big Data, SQL, Data Visualization</h6>
        </div>
        <div class="course-section color-2">
          <img src="/MVC/Views/images/course-business.jpg" alt="Chess board">
          <h4>Business</h4>
          <h5>Cursos<h5>
          <h6>Product Development, Business Development, Startup</h6>
        </div>
        <div class="course-section color-2">
          <img src="/MVC/Views/images/course-marketing.jpg" alt="Person using smart watch">
          <h4>Marketing</h4>
          <h5>Cursos<h5>
          <h6>Analytics, Content Marketing, Mobile Marketing</h6>
        </div>
      </div>
    </div>

    <div class="thesis-exhibit color-2">
      <div class="text-container">
          <h3>Exhibicion</h3>
      </div>
      <div class="full-width">
        <div class="left">
          <div class="video-container">
            <video controls>
              <source src="/MVC/Views/videos/thesis.mp4" type="video/mp4" alt="Video Presentation">
            </video>
          </div>
          <div class="text-container">
            <h2>Reimagina</h2>
            <p>"Curabitur vitae libero in ipsum poritor consequat. Aliquam et commodo lectus, nec consequat neque. Sed non accumsan urna. Phasellus sed consequat ex. Etiam eget magna laoreet, efficitur dolor consequat, tristique ligula."</p>
          </div>
        </div>
        <div class="right">
          <div class="newsfeed-block">
            <div class="image-container">
              <img src="/MVC/Views/images/thesis-fisma.jpg" alt="">
            </div>
            <div class="text-container">
              <h3>Diseños Demostrativos y Galletas 🍪</h3>
              <p>Demostracion de diseñadores sobre un nuevo prototipo, se serviran galletas caseras.</p>
            </div>
          </div>
          <div class="newsfeed-block">
            <div class="image-container">
              <img src="/MVC/Views/images/thesis-now-and-then.jpg" alt="">
            </div>
            <div class="text-container">
              <h3>Ahora y Despues</h3>
              <p>Investigacion sobre la ciudad</p>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>
    <footer class="color-1">
      <div class="footer-left">
        <p>©2019 Colmar Hotel. Derechos Reservados</p>
      </div>
      <div class="footer-right">
        <a href="">Terminos</a>
        <a href="">Privacidad</a>
      </div>
    </footer>

  </body>
</html>
