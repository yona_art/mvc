<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>SignUp</title>
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="forms.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

</head>

<body>
    <script src="Js/AuthQuerys.js" type="text/javascript"> </script>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Menu</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="/MVC/Views/Index">Menu Principal <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/MVC/Views/Login">Iniciar Sesion</a>
                </li>
              </ul>
            </div>
          </nav>
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4 d-flex justify-content-center">Bienvenido! Desea Registrase </h1>
            <p class="lead d-flex justify-content-center">Para poder acceder a reservaciones es necesario crear una
                cuenta.</p>
            <hr class="my-4">
            <div class="row">
                <div class="col-3"></div>
                <div class="col-6">
                    <div class="alert alert-danger  d-flex justify-content-center" id="alerts"
                    style="visibility: hidden;" role="alert">
                    Revise el usuario o email pueden estar repetidos
                </div>
                    <form id="register">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Username" aria-label="Username"
                                aria-describedby="basic-addon1" name="username">
                        </div>
                        <div class="input-group mb-3">
                            <input type="email" class="form-control" placeholder="Email" aria-label="email"
                                aria-describedby="basic-addon1" name="email">
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" class="form-control" placeholder="Contraseña" aria-label="Contraseña"
                                aria-describedby="basic-addon1" name="password">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nombre</span>
                            </div>
                            <input type="text" aria-label="Nombre" class="form-control" placeholder="Nombre" name="nom">
                            <input type="text" aria-label="pat" class="form-control" placeholder="Paterno" name="pat">
                            <input type="text" aria-label="mat" class="form-control" placeholder="Materno" name="mat">
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" pattern="[0-9]+" class="form-control" placeholder="Telefono 449-000-00-00" aria-label="telefono"
                                aria-describedby="basic-addon1" name="phone">
                        </div>
                        <div class="input group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Residencia</span>
                            </div>
                            <input type="text" aria-label="calle" class="form-control" placeholder="Calle" name="street">
                            <input type="text" aria-label="num" class="form-control" placeholder="Num Domicilio" name="num">
                            <input type="text" aria-label="num_int" class="form-control" placeholder="Num Interior" name="num_int" pattern="[0-9]+" >
                            <input type="text" aria-label="colonia" class="form-control" placeholder="Colonia" name="col" pattern="[0-9]+">
                            <input type="text" aria-label="cp" class="form-control" placeholder="Codigo Postal" name="cp" pattern="[0-9]+">

                        </div>
                        <div class="input-group mb-3">
                            <input type="text" pattern="[0-9]" class="form-control" placeholder="CURP" aria-label="curp" name="curp"
                                aria-describedby="basic-addon1" required
                                minlength="16" maxlength="18" size="18">
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" pattern="[0-9]" class="form-control" placeholder="Clave de Elector " aria-label="clave" name="clave"
                                aria-describedby="basic-addon1" required
                                minlength="16" maxlength="18" size="18">
                        </div>
                        <!-- dirección de residencia (calle, numero, numero interior, colonia, código postal), curp, clave de elector.-->
                        <div class="input-group mb-3 d-flex justify-content-center">
                            <input type="submit" value="Registrarse" class="btn btn-primary">
                        </div>
                    </form>
                </div>
                <div class="col-3"></div>
            </div>
        </div>
    </div>
</body>

</html>


