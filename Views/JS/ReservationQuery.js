$(document).ready(function () {
    var diffDays = 0;
    var totalcost = 0;

    $('#to').datepicker({
        dateFormat: 'yy-mm-dd',
        uiLibrary: 'bootstrap',
        locale: 'es-es',
    });

    $("#from").datepicker({
        dateFormat: 'yy-mm-dd',
        uiLibrary: 'bootstrap',
        locale: 'es-es',
    }).bind("change", function (e) {
        var date2 = $('#from').datepicker('getDate', '+1d');
        date2.setDate(date2.getDate() + 1);
        $('#to').datepicker('setDate', date2);
        $("#to").datepicker("option", "minDate", date2);
        var a = $("#datepicker_start").datepicker('getDate').getTime(),
            b = $("#datepicker_end").datepicker('getDate').getTime(),
            c = 24 * 60 * 60 * 1000;
        diffDays = Math.round(Math.abs((a - b) / (c)));
    });

    $('#save').click(function (e) {
        e.preventDefault();
        getReservationsByDates();
    });

    function getReservationsByDates() {
        var data = {
            action: "getAllAviableRooms",
            module: "Room",
            data: {
                start_date: $('#from').val(),
                end_date: $('#to').val(),
            }
        }
        let template = '';
        $.post("/MVC/Router", data,
            function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    totalcost =(type.cost * diffDays);
                    data.data.forEach(type => {
                        template += `
                        <div class="col-4">
                        <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="..." alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Habitacion ${type.name}</h5>
                            <p class="card-text">Tipo: ${type.type} <br> Costo Total: ${totalcost} </p>
                            <a href="#" roomid="${type.id}" class="btn btn-primary room-item">Reservar</a>
                                </div>
                            </div>
                        </div>`;
                    });
                } else {
                    template += `
                        <div class="col-12">
                        <div class="card">
                        <div class="card-body d-flex justify-content-center" >
                        ${data.data.name}
                        </div>
                        </div>
                        </div>`;
                }
                $('#cards').html(template);
            },
            "json"
        );
    }


    // Espera a que el item sea selecionado para abrir modal de update
    $(document).on('click', '.room-item', (e) => {
        const element = $(this)[0].activeElement;
        var data = {
            action: "createReservation",
            module: "Reservation",
            data: {
                older: $('#older').val(),
                younger: $('#younger').val(),
                room_id: $(element).attr('roomId'),
                start_date: $('#from').val(),
                end_date: $('#to').val(),
                total: totalcost,
            }
        }
        $.post("/MVC/Router", data,
            function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    window.location.href = "/Index";
                }
            },
            "json"
        );

        e.preventDefault();
    });


});

