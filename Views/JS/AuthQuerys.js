$(document).ready(function () {

    // Constructor imaginario del documento 
    init();
    function init() {

    }

    $("#register").submit(function (e) {
        e.preventDefault(e);
        var formdata = $('#register').serializeArray();
        var data = {
            action: "register",
            data: {
                username: formdata[0].value,
                email: formdata[1].value,
                password: formdata[2].value,
                name: formdata[3].value,
                apt_pat: formdata[4].value,
                apt_mat: formdata[5].value,
                phone: formdata[6].value,
                street: formdata[7].value,
                num: formdata[8].value,
                num_int: formdata[9].value,
                col: formdata[10].value,
                curp: formdata[11].value,
                clave: formdata[12].value,
            }
        }
        if (formdata[2].value == "") {
            alert("Agrege Contraseña");
        } else {
            $.post("/MVC/AuthRouter", data,
                function (data, textStatus, jqXHR) {
                    if (data.status == 200) {
                        window.location.href = "/Index";
                    } else {
                        $("#alerts").css("visibility", "initial");
                    }
                },
                "json"
            );
        }
    });

    $("#login").submit(function (e) {
        e.preventDefault(e);
        var formdata = $('#login').serializeArray();
        var data = {
            action: "login",
            data: {
                username: formdata[0].value,
                password: formdata[2].value,
            }

        }
        $.post("/MVC/AuthRouter", data,
            function (data, textStatus, jqXHR) {
                if (data.status != 200) {
                    $('#login').trigger("reset");
                    $("#alerts").css("visibility", "initial");
                } else {
                    window.location.href = "/Index"
                }
            },
            "json"
        );
    });



    $("#logout").submit(function (e) {
        e.preventDefault(e);
        var data = {
            action: "logout",
            module: "Auth",
            data: {
                nothing: "",
            }

        }
        $.post("/MVC/Router", data,
            function (data, textStatus, jqXHR) {
                if (data.status != 200) {
                    
                } else {
                    window.location.href = "/Index"
                }
            },
            "json"
        );
    });



});