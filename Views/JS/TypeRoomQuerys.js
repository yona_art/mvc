$(document).ready(function () {
    let id = 0;
    init();
    function init() {
        updateTable();
    }

    $("#save").click(function (e) {
        e.preventDefault();
        var formdata = $('#addType').serializeArray();
        var data = {
            action: "createRoomType",
            module: "TypeRoom",
            data: {
                type: formdata[0].value,
                cost: formdata[1].value,
            }
        }
        if (formdata[0].value == "" && formdata[0].value == "") {
            $("#alerts").css("visibility", "initial");
        } else {
            resetForm();
            $("#exampleModalCenter").modal('hide');
            $.post("/MVC/Router", data,
                function (data, textStatus, jqXHR) {
                    if (data.status == 200) {
                        updateTable();
                    }
                },
                "json"
            );
        }
    });


    $("#update").click(function (e) {
        e.preventDefault();
        var formdata = $('#updateType').serializeArray();
        var data = {
            action: "updateRoomType",
            module: "TypeRoom",
            data: {
                id: id,
                type: formdata[0].value,
                cost: formdata[1].value,
            }
        }
        if (formdata[0].value == "" && formdata[0].value == "") {
            $("#alertupdate").css("visibility", "initial");
        } else {
            resetForm();
            $("#updateModal").modal('hide');
            $.post("/MVC/Router", data,
                function (data, textStatus, jqXHR) {
                    if (data.status == 200) {
                        updateTable();
                    }
                },
                "json"
            );
        }
    });

    function updateTable() {
        var data = {
            action: "getRoomTypes",
            module: "TypeRoom",
            data: {
                nothing: ""
            }
        }
        $.post("/MVC/Router", data,
            function (data, textStatus, jqXHR) {
                const types = data.data[0];

                let template = '';
                types.forEach(type => {
                    template += `
                    <tr typeId="${type.id}">
                    <td>${type.id}</td>
                    <td>
                    <a href="#" class="type-item">
                      ${type.type} 
                    </a>
                    </td>
                    <td>${type.cost}</td>
                    <td>
                      <button class="type-delete btn btn-danger">
                       Eliminar
                      </button>
                    </td>
                    </tr>`;
                });
                $('#types').html(template);
            },
            "json"
        );
    }

    function resetForm() {
        $('#addType').trigger("reset");
        $('#updateType').trigger("reset");
        $("#alerts").css("visibility", "hidden");
    }


    // Espera a que el item sea selecionado para abrir modal de update
    $(document).on('click', '.type-item', (e) => {
        const element = $(this)[0].activeElement.parentElement.parentElement;
        const idtype = $(element).attr('typeId');
        var data = {
            action: "getRoomTypeById",
            module: "TypeRoom",
            data: {
                id: idtype
            }
        }
        $.post('/MVC/Router', data,
            function (data, textStatus, jqXHR) {
                const type = data.data[0];
                id = type.id;
                $('#uptname').val(type.type);
                $('#uptcost').val(type.cost);
                $('#updateModal').modal('show');
            },
            "json"
        );
        e.preventDefault();
    });

    // Hace una peticion para eliminar el tipo
    $(document).on('click', '.type-delete', (e) => {
        if (confirm('Estas seguro de eliminarlo?')) {
            const element = $(this)[0].activeElement.parentElement.parentElement;
            const id = $(element).attr('typeId');
            var data = {
                action: "deleteRoomType",
                module: "TypeRoom",
                data: {
                    id: id
                }
            }
            $.post('/MVC/Router', data,
                function (data, textStatus, jqXHR) {
                    if(data.status == 200){
                        updateTable();
                    }
                },
                "json"
            );
        }
    });

});