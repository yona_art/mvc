<?php

class ActiveRecord
{

  protected $table = "";
  protected $aux_table = "";
  protected $con;
  protected $id;

  /**
   * Constructor
   * @param Database Conexión a la bd
   */
  function __construct(Database $con)
  {
    $this->con = $con;
  }

  /**
   * Convierte un valor a entero, si no es null
   * Si es valor es vacío devuelve null
   * @param any $val Valor
   * @return int $val Valor entero o null
   */
  protected function keyval($val): ?int
  {
    return is_null($val) || intval($val) == 0 ? null : intval($val);
  }

  /**
   * Convierte un valor a entero, si no es null
   * @param any $val Valor
   * @return int $val Valor entero o null
   */
  protected function intval($val): ?int
  {
    return is_null($val) ? null : intval($val);
  }

  /**
   * Convierte un valor a float, si no es null
   * @param any $val Valor
   * @return int $val Valor float o null
   */
  protected function floatval($val): ?float
  {
    return is_null($val) ? null : floatval($val);
  }

  /**
   * Convierte un valor a string, si no es null
   * @param any $val Valor
   * @return int $val Valor string o null
   */
  protected function strval($val): ?string
  {
    return is_null($val) ? null : strval($val);
  }

  /**
   * Convierte un valor a bool
   * @param any $val Valor
   * @return int $val Valor entero o null
   */
  protected function boolval($val): ?bool
  {
    return is_null($val) ? false : boolval($val);
  }

  /**
   * Ejecuta una query de insert
   * @param string $query Query
   * @param array $params Parámetros de la query
   * @return int $id Id del insert
   * @throws PDOException
   */
  protected function insert(string $query, array $params = []): int
  {
    try {
      $handle = $this->con->prepare($query);
      $handle->execute($params);
      return intval($this->con->lastInsertId());
    } catch (\PDOException $e) {
      throw $e;
    }
  }

  /**
   * Ejecuta una query de update
   * @param string $query Query
   * @param array $params Parámetros de la query
   * @throws PDOException
   */
  protected function update(string $query, array $params = []): void
  {
    try {
      $handle = $this->con->prepare($query);
      $handle->execute($params);
    } catch (\PDOException $e) {
      throw $e;
    }
  }

  /**
   * Ejecuta una query de delete
   * @param string $query Query
   * @param array $params Parámetros de la query
   * @throws PDOException
   */
  protected function delete(string $query, array $params = []): void
  {
    try {
      $handle = $this->con->prepare($query);
      $handle->execute($params);
    } catch (\PDOException $e) {
      throw $e;
    }
  }

  /**
   * Ejecuta una query
   * @param string $query Query
   * @param array $params Parámetros de la query
   * @param int $fetch Determina cómo PDO devuelve la fila. Opciones $fetch_options
   * @return Any Resultado de la query, de acuerdo a $fetch
   */
  protected function query(string $query, array $params = [], $fetch = \PDO::FETCH_OBJ)
  {
    $handle = $this->con->prepare($query);
    $handle->execute($params);
    return $handle->fetch($fetch);
  }

  /**
   * Ejecuta una query que regresa todos los resultados
   * @param string $query Query
   * @param array $params Parámetros de la query
   * @param int $fetch Determina cómo PDO devuelve la fila. Opciones $fetch_options
   * @return Any Resultado de la query, de acuerdo a $fetch
   */
  public function queryAll(string $query, array $params = [], int $fetch = \PDO::FETCH_OBJ): array
  {
    $handle = $this->con->prepare($query);
    $handle->execute($params);
    return $handle->fetchAll($fetch);
  }

    /**
   * Encuentra un registro basado en su id
   * @param int $id id de la tabla
   * @param array $colums Columnas de la tabla a regresar
   * @param int $fetch Determina cómo PDO devuelve la fila. Opciones $fetch_options
   * @return Any Resultado de la query, de acuerdo a $fetch. Si sólo se requiere una columna, regresa un único valor.
   */
  protected function find(int $id, array $columns = [], int $fetch = \PDO::FETCH_OBJ)
  {
    if (sizeof($columns) == 0) {
      $columns_query = "*";
    } else {
      $columns_query = implode(',', $columns);
    }

    $handle = $this->con->prepare("SELECT {$columns_query} from {$this->table} where id = ? limit 1");
    $handle->execute([$id]);

    if (sizeof($columns) == 1)
      return $handle->fetchColumn();
    else
      return $handle->fetch($fetch);
  }

}

