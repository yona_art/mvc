<?php

require_once "Controller.php";
require_once "Models/ResponseModel.php";
require_once "Models/TypeRoomModel.php";

class TypeRoomController extends Controller
{


    public function createRoomType($data): ResponseModel
    {
        $roomController = new TypeRoomModel($this->con, $data);
        $id = $roomController->addType();
        if ($id == 0) {
            return new ResponseModel(201, array());
        }
        return new ResponseModel(200, array());
    }

    public function getRoomTypes($data)
    {
        $roomController = new TypeRoomModel($this->con, $data);
        $items =  $roomController->getAll();

        if (sizeof($items) == 0) {
            return new ResponseModel(202, array());
        }
        return new ResponseModel(200, array($items));
    }

    public function getRoomTypeById($data)
    {
        $roomController = new TypeRoomModel($this->con, $data);
        $roomController->getById($data->id);
        return new ResponseModel(200, array($roomController));
    }

    public function updateRoomType($data)
    {
        $roomController = new TypeRoomModel($this->con, $data);
        $update =  $roomController->updateTypeRoom();
        if (!$update) {
            return new ResponseModel(204, array());
        } else {
            return new ResponseModel(200, array());
        }
    }

    public function deleteRoomType($data)
    {
        $roomController = new TypeRoomModel($this->con, $data);
        $update =  $roomController->deleteRoomType();
        if (!$update) {
            return new ResponseModel(205, array());
        } else {
            return new ResponseModel(200, array());
        }
    }
}
