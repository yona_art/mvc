<?php
require_once("Controller.php");
require_once("Models/ResponseModel.php");
require_once("Models/ReservationModel.php");

class ReservationController extends Controller
{

    public function createReservation($data)
    {
        $reservationController = new ReservationModel($this->con, $data);

        $id = $reservationController->addReservation();
        if($id > 0){
            return new ResponseModel(201, array());
        }
        return new ResponseModel(200, array());
     }

    public function getReservation($data)
    { 
        $reservationController = new ReservationModel($this->con, $data);
        $items =  $reservationController ->getAll();
        if(sizeof($items)){
            return new ResponseModel(201, array());
        }
        return new ResponseModel(200, array($items));
    }

    public function updateReservation($data)
    { 
        $reservationController = new ReservationModel($this->con, $data);
        $update =  $reservationController ->updateReservation();
        if(!$update){
            return new ResponseModel(202, array());
        } else {
            return new ResponseModel(200, array());
        }
    }

    public function deleteReservation($data)
    { 
        $reservationController = new ReservationModel($this->con, $data);
        $update =  $reservationController ->deleteReservation();
        if(!$update){
            return new ResponseModel(203, array());
        } else {
            return new ResponseModel(200, array());
        }
    }

    public function getReservationsIdsFromDates($data){
        $reservationController = new ReservationModel($this->con, $data);
        $items =  $reservationController ->getReservationsIdsFromDates();
        if(sizeof($items)){
            return new ResponseModel(204, array());
        }
        return new ResponseModel(200, array($items));
    }

    public function getReservationsByUserID($data){
        $reservationController = new ReservationModel($this->con, $data);
        $items =  $reservationController ->getByUserId($_SESSION["user_id"]);
        if(sizeof($items)){
            return new ResponseModel(204, array());
        }
        return new ResponseModel(200, array($items));
    }


    
    public function cancelReservation($data)
    { 
        $reservationController = new ReservationModel($this->con, $data);
        $update =  $reservationController ->cancelReservation();
        if($update){
            return new ResponseModel(200, array());
        } else {
            return new ResponseModel(205, array());
        }
    }

}
