<?php
class Controller
{
    /**
     * Conexión a la base de datos
     * @var Database
     */
    protected $con;

    function __construct(Database $con)
    {
        $this->con = $con;
    }

    /**
     * getter de $con
     * @return Database $conexión de la base de datos
     */
    public function getConnection(): Database
    {
        return $this->con;
    }

    /**
     * Regresa el permiso que requiere cierta función
     * @param string $action Función a la que se va a acceder
     * @return string $permission Permiso que requiere la función
     */
    public function getAccesTo(string $action): string
    {

        if (!isset($this->permissions[$action]))
            return '';

        return $this->permissions[$action];
    }
}
