<?php
require_once "Controllers/AuthController.php";
require_once "Classes/Database.class.php";
date_default_timezone_set('America/Mexico_City');

if (session_status() == PHP_SESSION_ACTIVE) {
  session_destroy();
}
session_start();
$config = include('config.php');


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  echo "Wrong Request Method";
  exit();
}

// $post = json_decode(file_get_contents("php://input"));
$post = $_POST;
if (empty($_POST)) {
  exit();
}

$data = json_decode(json_encode($post["data"]), FALSE);
$route = $config['base_route'];
$action = $post["action"];
echo $action;


//Crea una conexión a db para todas las peticiones

$connection = Database::getConnection($config['db']);

//Desactiva el autocommit
$connection->beginTransaction();

$AuthController = new AuthController($connection);
$AuthController->db_config = $config['db'];
$result = $AuthController->$action($data);

//Si el resultado fue correcto, guarda los cambios en bd
//Sino, regresa al punto anterior para no tener cambios a medias
$connection = $AuthController->getConnection();
if ($result->status == 200) {
  $connection->commit();
} else {
  $connection->rollback();
}

echo json_encode((object) $result);
